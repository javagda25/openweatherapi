package com.javagda25.openweather.model;

import lombok.*;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Humidity {

    @XmlAttribute(name = "value", required = true)
    @XmlSchemaType(name = "unsignedByte")
    protected short value;
    @XmlAttribute(name = "unit", required = true)
    protected String unit;

}