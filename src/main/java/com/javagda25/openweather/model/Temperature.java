package com.javagda25.openweather.model;

import lombok.*;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Temperature {

    @XmlAttribute(name = "value", required = true)
    protected BigDecimal value;
    @XmlAttribute(name = "min", required = true)
    protected BigDecimal min;
    @XmlAttribute(name = "max", required = true)
    protected BigDecimal max;
    @XmlAttribute(name = "unit", required = true)
    protected String unit;

}