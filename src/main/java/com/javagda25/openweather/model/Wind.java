package com.javagda25.openweather.model;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "speed",
        "gusts",
        "direction"
})
public class Wind {

    @XmlElement(required = true)
    protected Speed speed;
    @XmlElement(required = true)
    protected Object gusts;
    @XmlElement(required = true)
    protected Direction direction;
}