package com.javagda25.openweather.model;

import lombok.*;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "city",
        "temperature",
        "humidity",
        "pressure",
        "wind",
        "clouds",
        "visibility",
        "precipitation",
        "weather",
        "lastupdate"
})
@XmlRootElement(name = "current")
public class Current {

    @XmlElement(required = true)
    protected City city;
    @XmlElement(required = true)
    protected Temperature temperature;
    @XmlElement(required = true)
    protected Humidity humidity;
    @XmlElement(required = true)
    protected Pressure pressure;
    @XmlElement(required = true)
    protected Wind wind;
    @XmlElement(required = true)
    protected Clouds clouds;
    @XmlElement(required = true)
    protected Visibility visibility;
    @XmlElement(required = true)
    protected Precipitation precipitation;
    @XmlElement(required = true)
    protected Weather weather;
    @XmlElement(required = true)
    protected Lastupdate lastupdate;
}