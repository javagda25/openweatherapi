package com.javagda25.openweather.model;

import lombok.*;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Weather {

    @XmlAttribute(name = "number", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int number;
    @XmlAttribute(name = "value", required = true)
    protected String value;
    @XmlAttribute(name = "icon", required = true)
    protected String icon;

    /**
     * Gets the value of the number property.
     *
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     *
     */
    public void setNumber(int value) {
        this.number = value;
    }

    /**
     * Gets the value of the value property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the icon property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIcon(String value) {
        this.icon = value;
    }

}
