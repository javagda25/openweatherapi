package com.javagda25.openweather;

import com.javagda25.openweather.model.Current;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    private static final String URL = "http://api.openweathermap.org/data/2.5/weather?q=London&appid=30b3bf9233fc0f5d2a6fd232110ff283&mode=xml";

    public static void main(String[] args) {

        // Zadajemy użytkownikowi serię pytań o informacje pogodowe które chciałby otrzymać.
        // Pytania:
        //      - typ pogody:
        //                  obecna,  - https://openweathermap.org/current#current_XML
        //                  dzisiejsza (przyszła godzinowa), - https://openweathermap.org/api/hourly-forecast#XML
        //                  na 16 dni) - https://openweathermap.org/forecast16#XML
        //      - parametry które nas interesują (wiatr, temperatura... itd.)


        // jeśli użytkownik wybierze pogodą na 16 dni, to może wybrać jedną z poniższych opcji:
//              -temperatura
//              -nasłonecznienie
//              -wiatr
//              -deszcz
//              -ciśnienie
//
//        Twoim zadaniem jest wypisać w postaci 16 zapisów (dzień pod dniem) kolejne wartości tych pól.
//
//          Np.
//        dzień 1 - 15 st.
//        dzień 2 - 17 st.
//        dzień 3 - -20 st.
//          ...
//        dzień 16 - 14 st.
//
//        To samo dla innych parametrów.

    }
}
